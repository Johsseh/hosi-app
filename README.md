** HMS**
This project will automate the daily operations of campus hospital.The project keeps track of the staff and patient (in-patient, out-patient) details. 
It also takes care of the ward, medical, invoice and the doctor’s appointment
details.

The system generates the daily ward availability.This is an integrated Hospital Information System, which addresses all the major functional areas of multi-specialty hospitals. It enables better patient care, patient safety, patient confidentiality, efficiency, reduced costs and better management information system. It provides easy access to critical information thus enabling the management to take better decisions on time.This project deals with processing of each and every department in the hospital. This project sincerely aims to reduce the manual processing of each department.The Scope of the project takes care of the details of each and every department.

In the  **long run**  these details will give the doctor, staffs, specialists and patient details including their salary, attendance , doctor’s appointments and the billing system. The details of Doctor and staff help the hospital to maintain the record of every person. Their attendance details help them to know about their attentive presence while salary is calculated. The billing system provides an efficient way for calculating bill details of the patients.


# **The system is still developing** #
<!doctype html>
<html lang="en"><head>
    <meta charset="utf-8">
    <title>Immigration Border Management System</title>
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="lib/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="lib/font-awesome/css/font-awesome.css">

    <script src="lib/jquery-1.11.1.min.js" type="text/javascript"></script>

    

    <link rel="stylesheet" type="text/css" href="stylesheets/theme.css">
    <link rel="stylesheet" type="text/css" href="stylesheets/premium.css">

</head>
<body class=" theme-blue">

    <!-- Demo page code -->

    <script type="text/javascript">
        $(function() {
            var match = document.cookie.match(new RegExp('color=([^;]+)'));
            if(match) var color = match[1];
            if(color) {
                $('body').removeClass(function (index, css) {
                    return (css.match (/\btheme-\S+/g) || []).join(' ')
                })
                $('body').addClass('theme-' + color);
            }

            $('[data-popover="true"]').popover({html: true});
            
        });
    </script>
    <style type="text/css">
        #line-chart {
            height:300px;
            width:800px;
            margin: 0px auto;
            margin-top: 1em;
        }
        .navbar-default .navbar-brand, .navbar-default .navbar-brand:hover { 
            color: #fff;
        }
    </style>

    <script type="text/javascript">
        $(function() {
            var uls = $('.sidebar-nav > ul > *').clone();
            uls.addClass('visible-xs');
            $('#main-menu').append(uls.clone());
        });
    </script>

    <!-- Le HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <!-- Le fav and touch icons -->
    <link rel="shortcut icon" href="../assets/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="../assets/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="../assets/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="../assets/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="../assets/ico/apple-touch-icon-57-precomposed.png">
  

  <!--[if lt IE 7 ]> <body class="ie ie6"> <![endif]-->
  <!--[if IE 7 ]> <body class="ie ie7 "> <![endif]-->
  <!--[if IE 8 ]> <body class="ie ie8 "> <![endif]-->
  <!--[if IE 9 ]> <body class="ie ie9 "> <![endif]-->
  <!--[if (gt IE 9)|!(IE)]><!--> 
   
  <!--<![endif]-->

    <div class="navbar navbar-default" role="navigation">
<div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
  <a class="" href="index.html"><span class="navbar-brand"><span class="fa fa-paper-plane"></span> Moyale Border Management System</span></a></div>

        <div class="navbar-collapse collapse" style="height: 1px;">
          <ul id="main-menu" class="nav navbar-nav navbar-right">
            <li class="dropdown hidden-xs">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                    <span class="glyphicon glyphicon-user padding-right-small" style="position:relative;top: 3px;"></span> Admin
                    <i class="fa fa-caret-down"></i>                </a>

             <ul class="dropdown-menu">
                <li><a href="adminlogin.php">login</a></li>
                   
          </ul>
</div>
      </div>
    </div>
    

    <div class="sidebar-nav">
    <ul>
    <li><a href="#" data-target=".dashboard-menu" class="nav-header" data-toggle="collapse"><i class="fa fa-fw fa-dashboard"></i>Home<i class="fa fa-collapse"></i></a></li>
    <li><ul class="dashboard-menu nav nav-list collapse">
            <li><a href="index.html"><span class="fa fa-caret-right"></span> Main</a></li>
           <li ><a href="media.html"><span class="fa fa-caret-right"></span> Media</a></li>
             </ul></li>

   

        <li><a href="#" data-target=".accounts-menu" class="nav-header collapsed" data-toggle="collapse"><i class="fa fa-fw fa-briefcase"></i> Account <span class="label label-info">+3</span></a></li>
        <li><ul class="accounts-menu nav nav-list collapse">
            <li ><a href="sign-in.html"><span class="fa fa-caret-right"></span> Sign In</a></li>
            <li ><a href="sign-up.php"><span class="fa fa-caret-right"></span> Sign Up</a></li>
            <li ><a href="reset-password.html"><span class="fa fa-caret-right"></span> Reset Password</a></li>
    </ul></li>

       
        <li><a href="help.php" class="nav-header"><i class="fa fa-fw fa-question-circle"></i> Help</a></li>
            <li><a href="faq.php" class="nav-header"><i class="fa fa-fw fa-comment"></i> Faq</a></li>
      </ul>
    </div>

    <div class="content">
        <div class="header">
            
            <h1 class="page-title">Help</h1>
                    <ul class="breadcrumb">
            <li><a href="index.html">Home</a> </li>
            <li class="active">Help</li>
        </ul>

        </div>
        <div class="main-content">
            
<div class="faq-content">
    <div class="row">
        <div class="col-sm-9 col-md-9">
            <div class="search-well">
                <form class="form-inline" style="margin-top:0px;">
                    <input class="input-xlarge form-control" placeholder="Search Help..." id="appendedInputButton" type="text">
                    <button class="btn btn-default" type="button"><i class="fa fa-search"></i> Go</button>
                </form>
            </div>

            <div class="panel panel-default">
                <p class="panel-heading">Popular Topics</p>
                <div class="panel-body">
                    <ol>
                        <li><a href="#">What if I have a question?</a></li>
                        <li><a href="#">Where can I get support?</a></li>
                        <li><a href="#">How long does it take to get a response?</a></li>
                        <li><a href="#">Where are you located?</a></li>
                    </ol>
                </div>
            </div>

            <div class="panel panel-default">
                <p class="panel-heading">Support Available</p>
                <div class="panel-body">
                    <p>All the above questions are well answered in the FAQs page. Also there are additional materials that provide user support which include: user manuals, readme files and online help. </p>
                    <a href="faq.php" class="btn btn-primary">Start Now</a>
                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="panel panel-default">
                <p class="panel-heading">Popular Topics</p>
                <div class="panel-body">
                <div class="row" style="text-align: center;">
                    <div class="pull-left unstyled col-sm-4 col-md-4">
                        <p><a href="#" class="btn btn-default" style="width: 115px;">Get started</a></p>
                        <p><a href="#" class="btn btn-default" style="width: 115px;">Location</a></p>
                        <p><a href="#" class="btn btn-default" style="width: 115px;">Contacts</a></p>
                    </div>
                    <div class="pull-left unstyled col-sm-4 col-md-4">
                        <p><a href="#" class="btn btn-default" style="width: 115px;">Your Account</a></p>
                        <p><a href="#" class="btn btn-default" style="width: 115px;">Services</a></p>
                        <p><a href="#" class="btn btn-default" style="width: 115px;">Security </a></p>
                    </div>
                    
                  </div>
                    <div class="clearfix"></div>
                </div>
            </div>

           
        </div>
        <div class="col-sm-3 col-md-3">
            <div class="toc">
                <h3>Table of Contents</h3>
                <ol>
                    <li><a href="#">Get started</a></li>
                    <li><a href="#">Services</a></li>
                    <li><a href="#">Your Account</a></li>
                    <li><a href="#">Security and privacy</a></li>
                    <li><a href="#">Location</a></li>
					
                </ol>
            </div>
            <div class="toc">
                <h3>Contact Us</h3>
                <h4>By Phone</h4>
                <p>+254-123-333-4321</p>
                <h4>By Email</h4>
                <p><a href="#">Immigration@im.org</a></p>
                <h4>Address</h4>
                <address>
P.O BOX 1234-1110,<br>
IMMIGRATION DEPARTMENT,<br>
NAIROBI.
</address>
                <div>
                    <button class="btn btn-default"><i class="fa fa-facebook"></i></button>
                    <button class="btn btn-default"><i class="fa fa-twitter"></i></button>
                    <button class="btn btn-default"><i class="fa fa-linkedin"></i></button>
                </div>
            </div>
        </div>
</div>
</div>


            <footer>
                <hr>

            
                <p class="pull-right"></p>
                <p>©Copyright:All rights reserved to Immigration department </p>
            </footer>
        </div>
    </div>


    <script src="lib/bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript">
        $("[rel=tooltip]").tooltip();
        $(function() {
            $('.demo-cancel-click').click(function(){return false;});
        });
    </script>
    
  
</body></html>

<?php
include_once 'widgets/dbconfig.php';
?>
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>HMS system</title>
        <link href="css/bootstrap.min.css" rel="stylesheet" media="screen">
    </head>

<body>
<?php
include_once 'shortcodes/header.php';
?>

<div class="row">
    <div class="clearfix"></div>

    <div class="container">
        <a href="admin/add-data.php" class="btn btn-large btn-info"><i class="glyphicon glyphicon-plus"></i> &nbsp; Add Records</a>
    </div>

    <div class="clearfix"></div><br />

    <div class="container">
        <table class='table table-bordered table-responsive'>
            <tr>
                <th>#</th>
                <th>FirstName</th>
                <th>LastName</th>
                <th>Registration NO</th>
                <th>Telephone</th>
                <th>Email</th>
                <th>Course</th>
                <th>Password</th>
                <th colspan="2" align="center">Actions</th>
            </tr>
            <?php
            $query = "SELECT * FROM student";
            $records_per_page=3;
            $newquery = $crud->paging($query,$records_per_page);
            $crud->dataview($newquery);
            ?>
            <tr>
                <td colspan="7" align="center">
                    <div class="pagination-wrap">
                        <?php $crud->paginglink($query,$records_per_page); ?>
                    </div>
                </td>
            </tr>

        </table>


    </div>
</div>

<?php include_once 'shortcodes/footer.php'; ?>
</body>
</html>

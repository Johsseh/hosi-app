<!<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Home | H.M.S system</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <?php
    include_once ('shortcodes/header.php');
    ?>
    <!-- JWeka kitu hapa-->
    <div class="jumbotron">
        <h1>Hospital management system</h1>
        <p class="lead">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
            quis nostrud exercitation ullamco </p>
        <p><a class="btn btn-lg btn-success" href="login.php" role="button">Login</a></p>
    </div>

    <!-- Kujaza suff -->
    <div class="row">
        <div class="col-lg-4">
            <h2>More on the system</h2>

            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                quis nostrud exercitation ullamco  </p>
            <p><a class="btn btn-primary" href="about.php" role="button">About &raquo;</a></p>
        </div>
        <div class="col-lg-4">
            <h2>want to stay in touch!</h2>
            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
            <p><a class="btn btn-primary" href="contact.php" role="button">contact us &raquo;</a></p>
        </div>
        <div class="col-lg-4">
            <h2>Are a student?</h2>
            <p>Duis aute irure dolor in reprehenderit in voluptate velit esse
                cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <p><a class="btn btn-primary" href="contact.php" role="button">Cool hook &raquo;</a></p>
        </div>
    </div>

    <!-- attach footer -->
    <footer class="footer">
        <p>&copy; 2016 HMS software, Your health is our business.</p>
    </footer>

</div> <!-- /container -->


<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>


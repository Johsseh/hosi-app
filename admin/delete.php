<?php
// check if value was posted
// include database and object file
	include_once '../widgets/dbconfig.php';
	include_once '../widgets/data.inc.php';

	// get database connection
	$database = new database();
	$db = $database->dbConnection();

	// prepare product object
	$product = new Data($db);
	
	// set product id to be deleted
	$product->id = isset($_GET['id']) ? $_GET['id'] : die('Need Product ID');
	
	// delete the product
	if($product->delete()){
		echo "<script>location.href='all.php'</script>";
	}
	
	// if unable to delete the product
	else{
		echo "<script>alert('Failed to Deleted Data')</script>";
		
	}
?>
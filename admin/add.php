<?php
include_once '../widgets/dbconfig.php';

$database = new database();
$db = $database->dbConnection();

include_once '../widgets/data.inc.php';
$product = new Data($db);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Hospital</title>

    <!-- Bootstrap -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">
    <link href="../css/font-awesome.min.css" rel="stylesheet">


    <link href="../css/main.css" rel="stylesheet">


    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>

</head>
<body>

    <?php
    include_once '../shortcodes/header.php';
    ?>
    <section id="feature" >
            <div class="container">
                <div class="row">
                    <?php
                    include_once '../shortcodes/sidebar.php';
                    ?>


                    <div class="col-md-9">

                            <p>
                                <a class="btn btn-primary" href="all.php" role="button">Back</a>
                            </p><br/>
                            <?php
                            if ($_POST) {

                                $product->name = $_POST['name'];
                                $product->gender = $_POST['gender'];
                                $product->contactNum = $_POST['contact'];
                                $product->address = $_POST['address'];

                                if ($product->create()) {
                                    ?>
                                    <div class="alert alert-success alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                        <strong>Success!</strong>  <a href="all.php">View Data</a>.
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <div class="alert alert-danger alert-dismissible" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                        <strong>Fail!</strong>
                                    </div>
                                    <?php
                                }
                            }
                            ?>

                            <form method="post" class="form-horizontal">
                                <div class="row">
                                    <div class="form-group">
                                        <label for="fame" class="col-sm-2 control-label">Firstname</label>

                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="fame" placeholder="Enter Firstname">

                                        </div>

                                    <div class="form-group">
                                        <label for="lame" class="col-sm-2 control-label">Lastname</label>

                                        <div class="col-sm-4">
                                            <input type="text" class="form-control" name="lame" placeholder="Enter Lastname">
                                        </div>
                                            </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="username" class="col-sm-2 control-label">Username</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="user" placeholder="Enter username">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="Telephone" class="col-sm-2 control-label">Telephone</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="telephone" placeholder="Enter Phone No.">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="email" class="col-sm-2 control-label">Email</label>
                                    <div class="col-sm-10">
                                        <input type="email" class="form-control" name="email" placeholder="Enter your email">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="countries" class="col-sm-2 control-label">Sex</label>
                                    <div>
                                        <select id="sex" name="sex" >
                                            <option value="male">Male</option>
                                            <option value="female">Female</option>
                                        </select>
                                   </div>
                                </div>

                                <div class="form-group">
                                    <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                    <div class="col-sm-10">
                                        <input type="password" class="form-control" name="pass" placeholder="Password">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <input type="hidden" name="master_subjoin" value="1">
                                        <input type="submit" value="Register">
                                    </div>
                                </div>
                            </form>
                        <b>You already have an account?</b>
                        <a href="login.php">Login</a>


                    </div>
                </div>

            </div>
</div><!--/.container-->
        </section>
</div>

<script src="../js/jquery.min.js"></script>
<script src="../js/bootstrap.min.js"></script>
</body>
</html>